#!/usr/bin/env python3

import argparse
import re
import datetime
import time
import json
import os
import sys

import tweepy
import serial


TEMPERATURE_RE = re.compile(r'Temperature = (?P<temp>[-+]?(\d+(\.\d*)?|\.\d+)) C')


class ParseException(Exception):
    pass


class Measurement:
    def __init__(self, temperature, timestamp=None):
        self.temperature = temperature
        self.timestamp = timestamp

    def has_changed(self, other, threshold):
        return ((self.temperature < threshold and other.temperature > threshold) or
                (self.temperature > threshold and other.temperature < threshold))

    def was_long_ago(self, other):
        return (other.timestamp - self.timestamp).total_seconds() / 60 > 30

    def to_twitter(self, threshold):
        complaint = ", which is way too cold" if self.temperature < threshold else ""
        return "My room temperature is now {}{}".format(self.temperature,
                                                        complaint)


def get_temp(ser):
    line = ser.readline().decode('utf-8')
    match = re.match(TEMPERATURE_RE, line)
    if not match:
        raise ParseException("Couldn't parse '{}'".format(line))
    return float(match.group("temp"))


def parse_args():
    parser = argparse.ArgumentParser(description='Read temperature from Arduino'
                                                 + ' and post it to Twitter.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--device', '--dev', '-d',
                        action='store',
                        default='/dev/ttyACM0',
                        help='serial device for Arduino')

    parser.add_argument('--baud', '-b',
                        action='store',
                        default=9600,
                        help='baud rate for the serial device')

    parser.add_argument('--threshold', '-t',
                        action='store',
                        default=20.0,
                        help='threshold for cold temperature')

    parser.add_argument('--sleep', '-s',
                        action='store',
                        default=5,
                        help='sleep between measurements (in minutes)')

    return parser.parse_args()


def post_to_twitter(api, measurement, threshold):
    message = measurement.to_twitter(threshold)
    print("Posting '{}'".format(message))
    try:
        api.update_status(message)
    except tweepy.error.TweepError as e:
        if e.api_code == 187:
            # Duplicate status
            pass


def read_config():
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.json")) as f:
        conf = json.load(f)
    for field in ['consumer key', 'consumer secret', 'access token', 'access token secret']:
        if field not in conf:
            print("Configuration doesn't have required field '{}'".format(field))
            sys.exit(1)
    return conf


def get_twitter_api():
    conf = read_config()
    auth = tweepy.OAuthHandler(conf['consumer key'], conf['consumer secret'])
    auth.set_access_token(conf['access token'], conf['access token secret'])

    return tweepy.API(auth)


def main():
    args = parse_args()
    ser = serial.Serial(args.device, args.baud)

    api = get_twitter_api()

    last_measurement = None
    while True:
        try:
            measurement = Measurement(get_temp(ser), datetime.datetime.now())
        except ParseException:
            continue

        if last_measurement is None:
            pass
        elif not last_measurement.has_changed(measurement, args.threshold):
            print("--- Measured {} C, but last one was {}".format(measurement.temperature,
                                                                  last_measurement.temperature))
            time.sleep(args.sleep * 60)
            continue
        elif not last_measurement.was_long_ago(measurement):
            diff = (measurement.timestamp - last_measurement.timestamp).total_seconds() / 60
            print("--- Got {} C, but {} is not long enough".format(measurement.temperature, diff))
            time.sleep(args.sleep * 60)
            continue

        last_measurement = measurement
        post_to_twitter(api, last_measurement, args.threshold)
        time.sleep(args.sleep * 6 * 60)


if __name__ == "__main__":
    main()
