#include <math.h>

// Constants from https://cdn.sparkfun.com/datasheets/Sensors/Temp/ntcle100.pdf
// B_{25/85} = 3977;

const int V_in = 5;

const int DEBUG = 0;

double raw_to_v_out(int raw) {
    return raw * V_in / 1024.0;
}

double v_out_to_r_2(double v_out) {
    const int r_1 = 10000;

    return V_in / v_out * r_1 - r_1;
}

double get_temp(double r) {
    const float R_ref = 10000;

    const float A = 3.354016E-03;
    const float B = 2.569850E-04;
    const float C = 2.620131E-06;
    const float D = 6.383091E-08;

    const double ln = log(r / R_ref);

    double temp = 1 / (A + B * ln + pow(C * ln, 2) + pow(D * ln, 3));
    return temp - 273.15; // From K to C
}

void setup() {
    Serial.begin(9600);
}

void loop() {
    int raw = analogRead(0);
    double v_out = raw_to_v_out(raw);
    double r_2 = v_out_to_r_2(v_out);

    if (DEBUG) {
        Serial.print("Got v_out = ");
        Serial.print(v_out);
        Serial.print(", and r_2 = ");
        Serial.print(r_2);
        Serial.print(" -- ");
    }

    double temp = get_temp(r_2);
    Serial.print("Temperature = ");
    Serial.print(temp);
    Serial.println(" C");

    delay(60000);
}
