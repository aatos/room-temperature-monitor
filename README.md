## Room temperature monitor

Monitors your room temperature and posts a tweet when temperature lowers too much.

### Required materials

- Arduino
  - Breadboard
  - 10K Ω resistor
  - NTCLE100E3 10K Ω thermistor
- Raspberry PI (or some other machine to read Arduino's serial console)

### Installation

- Upload `temp-monitor.ino` to Arduino.

- Install required python libraries:
  ``` bash
  pip install -r requirements.txt
  ```

- Setup a twitter app and authorize an account to use it.

- Create `config.json` with the secrets:
  ``` json
  {
    "consumer key": "<consumer_key>",
    "consumer secret": "<consumer_secret>",
    "access token": "<access_token>",
    "access token secret": "<access_token_secret>"
  }
  ```
